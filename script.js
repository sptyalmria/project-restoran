const daftarMakanan = [
  {
    id: 1,
    gambar:"img/menu/Hot & Spicy Chicken 4pcs.jpg",
    nama:"Hot & Spicy Chicken",
    deskripsi:"Paket Ayam 4pcs.",
    harga: 65000,
  },
  {
    id: 2,
    gambar:"img/menu/Fire Chicken Bite 3pcs.jpg",
    nama:"Fire Chicken Bite",
    deskripsi:"Paket Ayam 3pcs.",
    harga: 30000,
  },
  {
    id: 3,
    gambar:"img/menu/Richicken 4pcs.jpg",
    nama:"Richicken",
    deskripsi:"Paket Ayam 4pcs.",
    harga: 63000,
  },
  {
    id: 4,
    gambar:"img/menu/Fire Chicken Wings 6pcs.jpg",
    nama:"Fire Chicken Wings",
    deskripsi:"Paket Ayam 6pcs.",
    harga: 46000,
  },
  {
    id: 5,
    gambar:"img/menu/Combo 2 Hot & Spicy Chicken.jpg",
    nama:"Hot & Spicy Chicken",
    deskripsi:"Paket Combo 2.",
    harga: 48000,
  },
  {
    id: 6,
    gambar:"img/menu/Combo 3 Fire Chicken Bite.jpg",
    nama:"Fire Chicken Bite",
    deskripsi:"Paket Combo 3.",
    harga: 50000,
  },
  {
    id: 7,
    gambar:"img/menu/Combo 2 Richicken.jpg",
    nama:"Richicken",
    deskripsi:"Paket Combo 2.",
    harga: 52000,
  },
  {
    id: 8,
    gambar:"img/menu/Potato pompom.jpg",
    nama:"Potato pompom",
    deskripsi:"Dessert",
    harga: 15000,
  },
  {
    id: 9,
    gambar:"img/menu/Cookies and Cream Cake In a Cup.jpg",
    nama:"Cookies and Cream",
    deskripsi:"Cake In a Cup.",
    harga: 20000,
  },
  {
    id: 10,
    gambar:"img/menu/Tiramisu Cake In a Cup.jpg",
    nama:"Tiramisu",
    deskripsi:"Cake In a Cup.",
    harga: 20000,
  },
  {
    id: 11,
    gambar:"img/menu/Frutarian Tea.jpg",
    nama:"Frutarian Tea",
    deskripsi:"Drink.",
    harga: 16000,
  },
  {
    id: 12,
    gambar:"img/menu/Pink Lava.jpg",
    nama:"Pink Lava",
    deskripsi:"Drink.",
    harga: 17000,
  },
];

//Map
const callbackMap = (item, index)=>{
  const elmnt = document.querySelector('#catalogMenu');

  elmnt.innerHTML += `
      <div class="col col-md-3 mb-3 mt-3">
        <div class="card">
          <img src="${item.gambar}" class="card-img-top" height="250px">
              <div class="card-body text-center">
                <h5 class="card-title">${item.nama}</h5>
                <p class="card-text">${item.deskripsi}</p>
                <p class="card-text">Harga: Rp${item.harga}</p>
                <button class="btn btn-danger">Buy Now</button>
              </div>
         </div>
      </div>
    `
  }

daftarMakanan.map(callbackMap);

//Filter
const buttonElmnt = document.querySelector('#button-search');
buttonElmnt.addEventListener('click', ()=>{
    const hasilPencarian = daftarMakanan.filter((item, index)=>{
                              const inputElmnt = document.querySelector('#input-keyword');
                              const namaItem = item.nama.toLowerCase();
                              const keyword = inputElmnt.value.toLowerCase();

                              return namaItem.includes(keyword);
                            }) 

  document.querySelector('#catalogMenu').innerHTML = '';

  hasilPencarian.map(callbackMap);

});

//Reduce
const jumlahPesanan = []
const buttonPesan = document.querySelectorAll('.btn-danger');
const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

buttonPesan.forEach((item, index)=>{
item.addEventListener('click', ()=>{
  jumlahPesanan.push(1);
  
  
  const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
    }, 0);


  elmntJumlahPesanan.innerHTML = hasil;
  })
});